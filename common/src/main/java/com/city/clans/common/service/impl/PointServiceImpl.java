package com.city.clans.common.service.impl;

import com.city.clans.common.repository.PointRepository;
import com.city.clans.common.model.Point;
import com.city.clans.common.service.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by alexander on 04.12.14.
 */
@Service
public class PointServiceImpl implements PointService {

    @Resource
    PointRepository pointRepository;

    public Point save(Point model) {
        return pointRepository.save(model);
    }

    public Point get(Long id) {
        return pointRepository.findOne(id);
    }

    public List<Point> getAll() {
        return pointRepository.findAll();
    }

    public void remove(Long id) {
        pointRepository.delete(id);
    }

    public void removeEntity(Point model) {
        pointRepository.delete(model);
    }

    public List<Point> getUserPoints(Long userId) throws ExecutionException, InterruptedException {
        Future<List<Point>> points = pointRepository.findByUserId(userId);


        return points.get();
    }
}
