package com.city.clans.common.repository;

import com.city.clans.common.model.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by alexander on 04.12.14.
 */
public interface PointRepository extends JpaRepository<Point, Long> {

    @Async
    Future<List<Point>> findByUserId(Long userId);

}
