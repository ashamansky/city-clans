package com.city.clans.common.model;

import javax.persistence.*;

/**
 * Created by alexander on 07.12.14.
 */
@Entity
@Table(name = "city")
public class City implements BaseModel{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "countryId")
    private Long countryId;


    public City() {}

    public City(String name, Long countryId) {
        this.name = name;
        this.countryId = countryId;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "countryId", insertable = false, updatable = false)
    private Country country;

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
