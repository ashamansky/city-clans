package com.city.clans.common.model;

import java.io.Serializable;

/**
 * Created by alexander on 09.12.14.
 */
public interface BaseModel  extends Serializable{

    public Long getId();

    public void setId(Long id);
}
