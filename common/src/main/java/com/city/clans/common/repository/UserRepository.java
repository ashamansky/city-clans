package com.city.clans.common.repository;


import com.city.clans.common.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.Future;

/**
 * Created by alexander on 03.12.14.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    @Async
    Future<User> findByUsername(String username);



}
