package com.city.clans.common.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexander on 03.12.14.
 */
@Entity
@Table(name="users")
public class User implements Serializable, BaseModel {


    {
        this.createDate = new Date();
    }

    public User() {

    }

    public User(String username, String email, String password) {
        this.username = username;
        this.password = password;
        this.email = email;
    }


    public User(String username, String email, String password, boolean enabled) {
        this(username, email, password);
        this.enabled = enabled;
    }

    public User(String username, String email, String password, boolean enabled, Set<Role> userRole) {
        this(username, email, password, enabled);
        this.userRole = userRole;
    }

    @Column(name="enabled", nullable = false)
    private boolean enabled;



    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private Set<Role> userRole = new HashSet<Role>(0);


    @Id
    @GeneratedValue
    @Column(name="id", nullable=false)
    private Long id;


    @Column(name="username", unique = true,
            nullable = false, length = 45)

    private String username;

    @Column(name="email", unique = true,
            nullable = false)

    private String email;

    @Column(name="password", nullable = false)
    private String password;

    @Column(name = "createDate", columnDefinition = "timestamp with time zone")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;


    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }


    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }


    public boolean isEnabled() {
        return this.enabled;
    }


    public Set<Role> getUserRole() {
        return userRole;
    }

    public void setUserRole(Set<Role> userRole) {
        this.userRole = userRole;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
