package com.city.clans.common.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by alexander on 14.12.14.
 */
@Entity
@Table(name = "roles",
        uniqueConstraints = @UniqueConstraint(columnNames = {"role", "username"}))
public class Role implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_role_id",
            unique = true, nullable = false)
    private Long userRoleId;

    @Column(name = "role", nullable = false)
    private String role;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="username", nullable = false)
    private User user;



    public Role() {

    }

    public Role(User user, String role) {
        this.user = user;
        this.role = role;
    }



    public Long getUserRoleId() {
        return this.userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
