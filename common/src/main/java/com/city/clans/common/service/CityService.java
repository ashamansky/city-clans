package com.city.clans.common.service;


import com.city.clans.common.model.City;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alexander on 07.12.14.
 */
public interface CityService extends GenericService<City,Long> {

    public List<City> getCitiesByCountryId(Long countryId) throws ExecutionException, InterruptedException;
}
