package com.city.clans.common.service;


import com.city.clans.common.model.Role;
import com.city.clans.common.model.User;

/**
 * Created by alexander on 17.07.15.
 */
public interface RoleService extends GenericService<Role, Long> {
    enum USER_ROLES {ROLE_USER, ROLE_ADMIN, ROLE_GUEST};
    Role addUserRole(User user, USER_ROLES role);
    void deleteUserRoles(User user);
}
