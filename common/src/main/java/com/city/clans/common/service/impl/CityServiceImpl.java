package com.city.clans.common.service.impl;

import com.city.clans.common.repository.CityRepository;
import com.city.clans.common.model.City;
import com.city.clans.common.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alexander on 07.12.14.
 */
@Service(value = "cityService")
public class CityServiceImpl implements CityService {
    
    @Resource
    CityRepository cityRepository;


    public City save(City model) {
        return cityRepository.save(model);
    }


    public City get(Long id) {
        return cityRepository.findOne(id);
    }


    public List<City> getAll() {
        return cityRepository.findAll();
    }


    public void remove(Long id) {
        cityRepository.delete(id);
    }


    public void removeEntity(City model) {
        cityRepository.delete(model);
    }


    public List<City> getCitiesByCountryId(Long countryId) throws ExecutionException, InterruptedException {
        return cityRepository.findByCountryId(countryId).get();
    }
}
