package com.city.clans.common.service;


import com.city.clans.common.exception.UserExistsException;
import com.city.clans.common.model.User;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alexander on 03.12.14.
 */
public interface UserService extends GenericService<User, Long>{



    User createUser(String username, String email, String password) throws UserExistsException, Exception;

    User findUserByUsername(String username) throws ExecutionException, InterruptedException;

    List<User> searchUsers(Long country, Long city, String Username, String surname, String name);
}
