package com.city.clans.common.service;

/**
 * Created by alexander on 25.11.14.
 */
public interface CryptService {
    public String cryptPassword(String password, String salt);
    public String cryptCookiesPassword(String login, String passwordHash);
    public String getSalt(String passwordHash);

}
