package com.city.clans.common.service;


import com.city.clans.common.model.Country;

/**
 * Created by alexander on 07.12.14.
 */
public interface CountryService extends GenericService<Country,Long> {
}
