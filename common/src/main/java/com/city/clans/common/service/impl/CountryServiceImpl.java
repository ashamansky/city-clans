package com.city.clans.common.service.impl;

import com.city.clans.common.repository.CountryRepository;
import com.city.clans.common.model.City;
import com.city.clans.common.model.Country;
import com.city.clans.common.service.CityService;
import com.city.clans.common.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alexander on 07.12.14.
 */
@Service(value = "countryService")
public class CountryServiceImpl implements CountryService {
    
    @Resource
    CountryRepository countryrepository;

    @Autowired
    CityService cityService;

    public Country save(Country model) {
        return countryrepository.save(model);
    }

    public Country get(Long id) {
        return countryrepository.findOne(id);
    }

    public List<Country> getAll() {
        return countryrepository.findAll();
    }


    public void remove(Long id) throws ExecutionException, InterruptedException {
        List<City> cityList = cityService.getCitiesByCountryId(id);
        for (City city : cityList) {
            cityService.remove(city.getId());
        }
        countryrepository.delete(id);
    }

    public void removeEntity(Country model) {

    }

}
