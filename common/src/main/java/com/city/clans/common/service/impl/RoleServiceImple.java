package com.city.clans.common.service.impl;

import com.city.clans.common.repository.RoleRepository;
import com.city.clans.common.model.Role;
import com.city.clans.common.model.User;
import com.city.clans.common.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * Created by alexander on 17.07.15.
 */
@Service
public class RoleServiceImple implements RoleService {

    private final String ROLE_NAME_USER = "ROLE_USER";
    private final String ROLE_NAME_ADMIN = "ROLE_ADMIN";
    private final String ROLE_NAME_GUEST = "ROLE_GUEST";

    @Resource
    RoleRepository roleRepository;


    public Role save(Role model) {
        return roleRepository.save(model);
    }

    public Role get(Long id) {
        return roleRepository.findOne(id);
    }

    public List<Role> getAll() {
        return roleRepository.findAll();
    }

    public void remove(Long id) {
        roleRepository.delete(id);
    }

    public void removeEntity(Role model) {
        roleRepository.delete(model);
    }

    public Role addUserRole(User user, USER_ROLES role) {
        Role userRole = null;
        switch (role) {
            case ROLE_USER: userRole = new Role(user, ROLE_NAME_USER); break;
            case ROLE_ADMIN: userRole = new Role(user, ROLE_NAME_ADMIN); break;
            default: userRole = new Role(user, ROLE_NAME_GUEST); break;
        }
        if (userRole != null) {
            userRole = roleRepository.save(userRole);
        }

        return userRole;
    }

    public void deleteUserRoles(User user) {
        Set<Role> roles = user.getUserRole();
        for(Role role : roles) {
            roleRepository.delete(role.getUserRoleId());
        }
    }
}
