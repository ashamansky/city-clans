package com.city.clans.common.repository;


import com.city.clans.common.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by alexander on 07.12.14.
 */
public interface CityRepository extends JpaRepository<City, Long> {

    @Async
    Future<List<City>> findByCountryId(Long countryId);
}
