package com.city.clans.common.service.impl;


import com.city.clans.common.repository.UserRepository;
import com.city.clans.common.model.User;
import com.city.clans.common.service.CryptService;
import com.city.clans.common.service.RoleService;
import com.city.clans.common.service.UserService;
import com.city.clans.common.exception.UserExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alexander on 03.12.14.
 */
@Service("userService")
public class UserServiceImpl implements UserService {


    @Resource
    private UserRepository userRepository;

    @Autowired
    private CryptService cryptService;

    @Autowired
    private RoleService roleService;


    public User save(User model) {
        return userRepository.save(model);
    }



    public User get(Long id) {
        return userRepository.findOne(id);
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public void remove(Long id) {
        userRepository.delete(id);
    }

    public void removeEntity(User model) {

        roleService.deleteUserRoles(model);
        userRepository.delete(model);
    }

    public User createUser(String username, String email, String password) throws Exception {
        //

        try {
            User user = userRepository.findByUsername(username).get();
            if (user != null) {
                throw new UserExistsException("User with username :" + user.getUsername() + " already exists");
            } else {
                user = new User(username, email, password);


                String salt = cryptService.getSalt(password);
                String cryptedPassword = cryptService.cryptPassword(password, salt);

                user.setPassword(cryptedPassword);

                user = userRepository.save(user);
                return user;
            }


        } catch (Exception e) {
            throw new Exception(e);
        }

    }


    public User findUserByUsername(String username) throws ExecutionException, InterruptedException {
        return userRepository.findByUsername(username).get();
    }

    public List<User> searchUsers(Long country, Long city, String username, String surname, String name) {

//        return userRepository.searchUsers(country, city, username, surname, name);
        return null;
    }

}
