package com.city.clans.common.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by alexander on 04.12.14.
 */
@Entity
@Table(name = "point")
public class Point implements BaseModel{

    {
        this.createDate = new Date();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private Long objectId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "objectId", insertable = false, updatable = false)
    private User user;


    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longtitude")
    private Double longtitude;


    @Column(name = "createDate", columnDefinition = "timestamp with time zone")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;


    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        if (createDate != null) {
            this.createDate = new Date(createDate.getTime());
        } else {
            this.createDate = null;
        }
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(Double longtitude) {
        this.longtitude = longtitude;
    }

    public Point() {}

    public Point(long objectId, double longtitude, double latitude) {
        this.objectId = objectId;
        this.longtitude = longtitude;
        this.latitude = latitude;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
