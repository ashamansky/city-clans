package com.city.clans.common.service;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alexander on 03.12.14.
 */
public interface GenericService<T, PK> {
    public T save(T model);
    public T get(PK id);
    public List<T> getAll();
    public void remove(PK id) throws ExecutionException, InterruptedException;
    public void removeEntity(T model);

}
