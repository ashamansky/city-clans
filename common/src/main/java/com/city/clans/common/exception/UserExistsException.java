package com.city.clans.common.exception;

/**
 * Created by alexander on 07.12.14.
 */
public class UserExistsException extends Exception {
    public UserExistsException(String errorMessage) {
        super(errorMessage);
    }

}
