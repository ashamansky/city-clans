package com.city.clans.common.repository;


import com.city.clans.common.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by alexander on 07.12.14.
 */
public interface CountryRepository extends JpaRepository<Country, Long> {

}
