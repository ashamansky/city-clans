package com.city.clans.common.service;

import com.city.clans.common.model.Point;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by alexander on 04.12.14.
 */
public interface PointService extends GenericService<Point, Long> {
    public List<Point> getUserPoints(Long userId) throws ExecutionException, InterruptedException;

}
