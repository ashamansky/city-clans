package com.city.clans.common.repository;


import com.city.clans.common.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by alexander on 17.07.15.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {



}
