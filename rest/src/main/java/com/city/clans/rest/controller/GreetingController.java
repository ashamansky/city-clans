package com.city.clans.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * Created by alexander on 14.09.15.
 */
@RestController
public class GreetingController {

    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public Map<String, String> greeting() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("test", "value");
        return map;
    }
}
